<?php
/**
 * Learning_NewPage
 *
 * @category     Learning
 * @package      Learning_NewPage
 * @author       Artjoms Petersons <artjoms.petersons@scandiweb.com>
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/NewTheme/M2Theme',
    __DIR__
);
