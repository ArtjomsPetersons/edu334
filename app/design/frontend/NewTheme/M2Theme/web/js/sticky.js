/**
 * @category design
 * @package  NewTheme_M2Theme
 * @author   Artjoms Petersons <artjoms.petersons@scandiweb.com>
 */
require([
    'jquery',
    'domReady'
], function ($, domReady) {

    const header = $('.page-header');
    const categories = $('.sections.nav-sections');
    const logo = $('.page-header .logo');
    const search = $('.block.block-search');
    const minicart = $('.minicart-wrapper');

    //State shows if functions 'goDown/goUp' already worked. Function can work only one time.
    let functionState = 0;

    //Animation for scrolling from 400+ px to 400- px
    function goUp() {
        let pos = 0;
        let id = setInterval(frame, 5);

        function frame() {
            if (pos === 70) {
                $('.header.content').show();
                $('.nested').show();
                logo.insertAfter('.action.nav-toggle');
                search.insertAfter(logo);
                minicart.insertBefore(search);
                $('.control input').removeClass('margin-top').addClass('no-margin-top');
                $('.actions .action.search').removeClass('margin-top').addClass('no-margin-top');
                $('.header.content .logo').addClass('logo-margin');
            }
            if (pos === 143) {
                clearInterval(id);
                $('.panel.header').show();
            } else {
                pos++;
                $('.header.content').css('height', pos - 43 + 'px');
                $('.panel.wrapper').css('height', pos - 100 + 'px');
            }
        }
    }

    //Animation for scrolling from 400- px to 400+ px
    function goDown() {
        let pos = 143;
        let id = setInterval(frame, 5);

        function frame() {
            if (pos === 100) {
                $('.panel.header').hide();
            }
            if (pos === 70) {
                logo.insertBefore('.level0.nav-1.category-item.first.level-top.ui-menu-item');
                search.insertAfter('.level0.nav-2.category-item.last.level-top.ui-menu-item');
                minicart.insertBefore(search);
                $('.control input').removeClass('no-margin-top').addClass('margin-top');
                $('.actions .action.search').removeClass('no-margin-top').addClass('margin-top');
                $('.action.showcart').addClass('margin-top');
                $('.navigation .logo').addClass('no-margin');
                $('.header.content').hide();
                $('.nested').hide();
                $('.navigation ul .logo img').addClass('logo-height');
                $('.header.content .logo').removeClass('logo-margin');
            }
            if (pos === 0) {
                clearInterval(id);
            } else {
                pos--;
                $('.header.content').css('height', pos - 43 + 'px');
                $('.panel.wrapper').css('height', pos - 100 + 'px');
                console.log('Position: ' + pos);
            }
        }
    }

    domReady(function () {
        $(window).scroll(function () {
            if (($('.header.content').is(':visible')) && ($('.nested').is(':visible'))) {
                if ($(window).scrollTop() >= 400) {
                    console.log('workDown')
                    if (functionState === 1 || functionState === 0) {
                        goDown();
                        functionState = 2;
                    }
                }
            } else if ($('.header.content').is(':hidden') && $('.nested').is(':hidden')) {
                //Added 143 px to scroll top because part of header disappear and window height become smaller
                if ($(window).scrollTop() + 143 < 400) {
                    console.log('workUp')
                    if (functionState === 2 || functionState === 0) {
                        goUp();
                        functionState = 1;
                    }
                }
            }
        });
    })

    //Change of header layout. Categories added to header.
    domReady(function () {
        header.append(categories);
        header.addClass('sticky');
    })


});
