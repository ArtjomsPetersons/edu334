/**
 * @category design
 * @package  NewTheme_M2Theme
 * @author   Artjoms Petersons <artjoms.petersons@scandiweb.com>
 */
require([
    'jquery',
    'slick'
], function ($) {
    $(document).ready(function () {
        $('.product-items').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 2,
            prevArrow:'<div class="prev-arrow"></div>',
            nextArrow:'<div class="next-arrow""></div>',
            responsive: [
                {
                    breakpoint: 1279,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 1023,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        centerMode:true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
});
