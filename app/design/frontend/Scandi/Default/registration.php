<?php
/**
 * Learning_NewPage
 *
 * @category     Learning
 * @package      Learning_NewPage
 * @author       Artjoms Petersons <artjoms.petersons@scandiweb.com>
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'frontend/Scandi/Default',
    __DIR__
);
