<?php
/**
 * Learning_NewPage
 *
 * @category     Learning
 * @package      Learning_NewPage
 * @author       Artjoms Petersons <artjoms.petersons@scandiweb.com>
 */

namespace Learning\NewPage\Setup;

use Magento\Cms\Model\PageFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Filesystem\Driver\File;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var File
     */
    private $file;

    /**
     * Construct
     *
     * @param PageFactory $pageFactory
     * @param File $file
     */
    public function __construct(
        PageFactory $pageFactory,
        File $file
    ) {
        $this->pageFactory = $pageFactory;
        $this->file = $file;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws FileSystemException
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $content=$this->file->fileGetContents('app/code/Learning/NewPage/Setup/custom.phtml');
        $page = $this->pageFactory->create();
        $page->setTitle('Example CMS page')
                ->setIdentifier('example-cms-page')
                ->setIsActive(true)
                ->setPageLayout('1column')
                ->setStores([0])
                ->setContent($content)
                ->save();
    }
}
